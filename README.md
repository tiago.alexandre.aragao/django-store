# Django Store

[![Linkedin Badge](https://img.shields.io/badge/-LinkedIn-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/tiago-alexandre-gcp/)](https://www.linkedin.com/in/tiago-alexandre-gcp/)
[![Youtube Badge](https://img.shields.io/badge/-YouTube-ff0000?style=flat-square&labelColor=ff0000&logo=youtube&logoColor=white&link=https://www.youtube.com/channel/UCvCoeip4uqwHXkwCqcN7pbA/videos)](https://www.youtube.com/channel/UCvCoeip4uqwHXkwCqcN7pbA/videos)


> An implementation of [Django and Django Rest Framework](https://www.django-rest-framework.org/)

## Back-end Overview

[![Watch the video](https://gitlab.com/tiago.alexandre.aragao/django-store/-/raw/develop/documentation/images/vd2.jpg)](https://www.youtube.com/watch?v=nQ1cokNJVnw)


## Front-end Overview
[![Watch the video](https://gitlab.com/tiago.alexandre.aragao/django-store/-/raw/develop/documentation/images/vd1.jpg)](https://www.youtube.com/watch?v=8O0_npuERVo)


## Browser Support


| IE9 | IE10 | IE11 | Chrome | Opera | Firefox | Safari | Chrome (Android) | Mobile Safari |
|-----|------|------|--------|-------|---------|--------|------------------|---------------|
| B   | A    | A    | A      | A     | A       | A      | B                | B             |

A-grade browsers are fully supported. B-grade browsers will gracefully degrade
to our CSS-only experience.


## 1 - Download / Clone

Clone the repo using Git:

```bash
git clone https://gitlab.com/tiago.alexandre.aragao/django-store
```


## 2 - Docker Build

```bash
sudo ./start.sh
```

## 3 - Install Remote Container 

Search in extensions [Remote Container](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) or download

```bash
ext install ms-vscode-remote.remote-containers
```

## 4 - Run the Remote Container 

Click on Reopen in container

![Reopen in Container](documentation/images/remote-containers-reopen.png)

> The Visual Studio Code Remote - Containers extension lets you use a Docker container as a full-featured development environment. It allows you to open any folder inside (or mounted into) a container and take advantage of Visual Studio Code's full feature set. A devcontainer.json file in your project tells VS Code how to access (or create) a development container with a well-defined tool and runtime stack. This container can be used to run an application or to separate tools, libraries, or runtimes needed for working with a codebase.

> Workspace files are mounted from the local file system or copied or cloned into the container. Extensions are installed and run inside the container, where they have full access to the tools, platform, and file system. This means that you can seamlessly switch your entire development environment just by connecting to a different container.



## 5 - Provision the database and create admin user

```bash
 ./init.sh
```

> This script will migrate the tables and create the admin user


## 6 - Open Django Page

The website url:

http://localhost and http://localhost/admin

user: admin 
password: 123456789


## API With Insomnia

Use the api with insomnia app: [Download config file](documentation/files/Insomnia.json) 


## API With Swagger

Use swagger: http://localhost/swagger
[![Swagger](https://gitlab.com/tiago.alexandre.aragao/django-store/-/raw/develop/documentation/images/swagger.png)](http://localhost/swagger/)

## Feature requests

Redis is an in-memory database that can be used for caching. To begin you’ll need a Redis server running either locally or on a docker machine.




## License

© Tiago Aragão, 2022. 

from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.permissions import IsAuthenticated
from registration.api.serializers import ProductSerializer
from registration.models.product import Product


class ProductViewSet(ModelViewSet):
    premission_classes = (IsAuthenticated, )
    #authentication_classes = (TokenAuthentication)

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filterset_fields = ['in_stock']
    search_fields = ['title']

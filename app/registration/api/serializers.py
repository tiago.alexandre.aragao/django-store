from rest_framework import serializers
from registration.models.product import Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'id', 'title', 'description', 'cost', 'price', 'sale_price', 'in_stock', 'slug'
        ]

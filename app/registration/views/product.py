from django.shortcuts import render
from registration.models.product import Product


def product(request, slug):

    template_name = 'product.html'

    product = Product.objects.get(slug=slug)

    return render(
        request,
        template_name,
        context={'product': product}
    )

from django.test import TestCase
from .models.product import Product


class DbTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super(DbTest, cls).setUpClass()

        cls.produtc = Product(
            title='Unit Teste',
            description='Unit Teste',
            slug='unit-test',
            price='0.05',
            sale_price='0.03',
            cost='1.00',
            in_stock=False,
        )

        cls.produtc.save()


class ProductModelTestCase(DbTest):
    def test_create_title_product(self):
        self.assertEqual(self.produtc.title, 'Unit Teste')

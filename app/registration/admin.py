from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from django.utils.html import mark_safe
from registration.models.product import Product
from registration.models.category import Category


@admin.register(Product)
class ProductAdmin(ImportExportModelAdmin, admin.ModelAdmin):

    def img(self, obj):
        if obj.image != '' and obj.image != None:
            return mark_safe('<img src="/media/{}"  width="60px" height="auto" />'.format(obj.image))
        else:
            return mark_safe('<img src="/media/img/default.png"  width="60px" height="auto" />')

    def name(self, obj):
        return mark_safe('<div>{}</div>'.format(obj.title))

    def price(self, obj):
        return mark_safe('<div>{}</div>'.format(obj.sale_price))

    def in_stock(self, obj):
        return mark_safe('<div>{}</div>'.format("Yes" if obj.in_stock else "No"))

    img.short_description = ''

    view_on_site = True
    show_close_button = True
    search_fields = ['title', 'description']
    list_filter = ['in_stock', 'sale_price']
    list_display = ['img', 'name', 'price', 'in_stock']
    readonly_fields = ['img']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    view_on_site = True
    show_close_button = True

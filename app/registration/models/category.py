from django.db import models
from django.urls import reverse


class Category(models.Model):
    title = models.CharField(max_length=120)
    slug = models.SlugField(unique=True, blank=True, null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return "/category_detail/%i" % self.slug

    @property
    def get_products(self):
        return Category.objects.filter(category=self.title)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = "Categories"

from django.db import models
from .category import Category


class Product(models.Model):
    title = models.CharField(max_length=120)
    description = models.CharField(max_length=255)
    slug = models.CharField(
        unique=True,
        max_length=255,
        blank=False,
        null=False
    )
    cost = models.FloatField(blank=False, null=False, default='0')
    price = models.FloatField(blank=False, null=False, default='0')
    sale_price = models.FloatField(blank=False, null=False, default='0')
    ean = models.CharField(max_length=13, default='0000000000000')
    in_stock = models.BooleanField(blank=False, null=False, default=False)
    categories = models.ForeignKey(
        Category,
        null=True,
        blank=True,
        default='',
        on_delete=models.SET_NULL
    )
    image = models.ImageField(
        upload_to='product/images/',
        blank=True,
        null=True
    )
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def get_absolute_url(self):
        return f"/product/{self.slug}"

    def sale_prices(self):
        return "${:,.2f}".format(self.sale_price)

    def prices(self):
        return "${:,.2f}".format(self.price)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'product'
        verbose_name_plural = "Products"

#!/bin/bash
python manage.py makemigrations
python manage.py migrate
python manage.py migrate --run-syncdb

echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@gmail.com', '123456789')" | python manage.py shell
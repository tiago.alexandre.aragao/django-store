
import requests
from django.test import TestCase


class AccessTest(TestCase):
    def test_home_provisioned(self):
        headers = {
            'Accept': '*/*',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:99.0) Gecko/20100101 Firefox/99.0'
        }

        url = 'http://localhost:8000'
        response = requests.get(url, headers=headers,)
        status = response.status_code

        assert status == 200

    def test_admin_access_security(self):
        headers = {
            'Accept': '*/*',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:99.0) Gecko/20100101 Firefox/99.0'
        }

        url = 'http://localhost:8000/admin/registration/product/'
        response = requests.get(url, headers=headers, allow_redirects=False)
        status = response.status_code
        # redirect to login - is ok
        assert status == 302

from django.shortcuts import render
from registration.models.product import Product


def home(request):

    template_name = 'home.html'

    if 'q' in request.GET:
        param = request.GET.get("q", "")
        products = Product.objects.filter(title__icontains=param)
    else:
        products = Product.objects.all().order_by('-id')

    return render(
        request,
        template_name,
        context={'products': products}
    )

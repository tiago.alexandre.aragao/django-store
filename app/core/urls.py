from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_simplejwt import views as jwt_views
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view
from rest_framework_simplejwt.authentication import JWTAuthentication

#from rest_framework.schemas import get_schema_view
from rest_framework_swagger.renderers import SwaggerUIRenderer, OpenAPIRenderer

from registration.api.viewsets import ProductViewSet
from home.views import home
from registration.views.product import product

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        use_session_auth=False,
        title="Django Store",
        default_version='v1',
        description="Store App",
        terms_of_service="https://gitlab.com/tiago.alexandre.aragao/django-store",
        contact=openapi.Contact(email="tiago.alexandre.aragao@gmail.com"),
        license=openapi.License(name=""),
    ),
    public=True,
    permission_classes=[permissions.IsAuthenticatedOrReadOnly],
    authentication_classes=[JWTAuthentication]
)

router = routers.DefaultRouter()
router.register('products', ProductViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home),
    re_path(r'^products/search/', home),
    re_path(r'^product/(?P<slug>[\w-]+)/$', product),
    # API
    re_path(
        r'^swagger/$',
        schema_view.with_ui(
            'swagger',
            cache_timeout=0
        ),
        name='schema-swagger-ui'
    ),
    re_path(
        r'redoc/$',
        schema_view.with_ui(
            'redoc',
            cache_timeout=0
        ),
        name='schema-redoc'
    ),
    path('api/', include(router.urls)),
    path(
        'api/token/',
        jwt_views.TokenObtainPairView.as_view(),
        name='token_obtain_pair'
    ),
    path(
        'api/token/refresh/',
        jwt_views.TokenRefreshView.as_view(),
        name='token_refresh'
    ),
    path(
        'api/token/verify/',
        jwt_views.TokenVerifyView.as_view(),
        name='token_verify'
    )
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
